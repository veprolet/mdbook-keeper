use std::{mem, path::PathBuf};

use pulldown_cmark::{CodeBlockKind, Event, HeadingLevel, Parser, Tag};
use sha2::{Digest, Sha256};

#[derive(Debug)]
pub enum Buffer {
    None,
    Code(Vec<String>),
    Heading(String),
}

fn get_hash(contents: &str) -> String {
    let mut hasher = Sha256::new();

    hasher.update(contents.as_bytes());

    base64_url::encode(hasher.finalize().as_slice())
}

pub fn extract_tests_from_string(s: &str, file_stem: &str, path: Option<&PathBuf>) -> (Vec<Test>, Option<String>) {
    let mut tests = Vec::new();
    let mut buffer = Buffer::None;
    let parser = Parser::new(s);
    let mut section = None;
    let mut code_block_start = 0;
    // Oh this isn't actually a test but a legacy template
    let mut old_template = None;

    for (event, range) in parser.into_offset_iter() {
        let line_number = bytecount::count(&s.as_bytes()[0..range.start], b'\n');
        match event {
            Event::Start(Tag::Heading(level, ..)) if level < HeadingLevel::H3 => {
                buffer = Buffer::Heading(String::new());
            }
            Event::End(Tag::Heading(level, ..)) if level < HeadingLevel::H3 => {
                let cur_buffer = mem::replace(&mut buffer, Buffer::None);
                if let Buffer::Heading(sect) = cur_buffer {
                    section = Some(sanitize_test_name(&sect));
                }
            }
            Event::Start(Tag::CodeBlock(CodeBlockKind::Fenced(ref info))) => {
                let code_block_info = parse_code_block_info(info);
                if code_block_info.is_rust {
                    buffer = Buffer::Code(Vec::new());
                }
            }
            Event::Text(text) => {
                if let Buffer::Code(ref mut buf) = buffer {
                    if buf.is_empty() {
                        code_block_start = line_number;
                    }
                    buf.extend(text.lines().map(|s| format!("{}\n", s)));
                } else if let Buffer::Heading(ref mut buf) = buffer {
                    buf.push_str(&text);
                }
            }
            Event::End(Tag::CodeBlock(CodeBlockKind::Fenced(ref info))) => {
                let code_block_info = parse_code_block_info(info);
                if let Buffer::Code(buf) = mem::replace(&mut buffer, Buffer::None) {
                    if code_block_info.is_old_template {
                        old_template = Some(buf.into_iter().collect())
                    } else {
                        let name = if let Some(ref section) = section {
                            format!("{}_sect_{}_line_{}", file_stem, section, code_block_start)
                        } else {
                            format!("{}_line_{}", file_stem, code_block_start)
                        };
                        tests.push(Test {
                            name,
                            ignore: code_block_info.ignore,
                            compile_fail: code_block_info.compile_fail,
                            no_run: code_block_info.no_run,
                            should_panic: code_block_info.should_panic,
                            template: code_block_info.template,
                            output: code_block_info.output,
                            output_file: code_block_info.output_file.and_then(|file| path.map(|path| PathBuf::from("./src/").join(path.join(file)))),
                            hash: get_hash(&buf.join("\n")),
                            text: buf,
                        });
                    }
                }
            }
            _ => (),
        }
    }
    (tests, old_template)
}

pub fn sanitize_test_name(s: &str) -> String {
    s.to_ascii_lowercase()
        .chars()
        .map(|ch| {
            if ch.is_ascii() && ch.is_alphanumeric() {
                ch
            } else {
                '_'
            }
        })
        .collect::<String>()
        .split('_')
        .filter(|s| !s.is_empty())
        .collect::<Vec<_>>()
        .join("_")
}

pub fn parse_code_block_info(mut tokens: &str) -> CodeBlockInfo {
    let mut seen_rust_tags = false;
    let mut seen_other_tags = false;
    let mut info = CodeBlockInfo {
        is_rust: false,
        should_panic: false,
        compile_fail: false,
        ignore: false,
        no_run: false,
        is_old_template: false,
        template: None,
        output: false,
        output_file: None
    };

    loop {
        let token;
        (token, tokens) = match parse_token(tokens) {
            Some(pair) => pair,
            None => { break; }
        };

        match token {
            Token::Tag("rust") => {
                info.is_rust = true;
                seen_rust_tags = true
            }
            Token::Tag("should_panic") => {
                info.should_panic = true;
                seen_rust_tags = true
            }
            Token::Tag("ignore") => {
                info.ignore = true;
                seen_rust_tags = true
            }
            Token::Tag("compile_fail") => {
                info.compile_fail = true;
                seen_rust_tags = true;
            }
            Token::Tag("no_run") => {
                info.no_run = true;
                seen_rust_tags = true;
            }
            Token::Tag("skeptic-template") => {
                info.is_old_template = true;
                seen_rust_tags = true
            }
            Token::Tag("output") => {
                info.output = true;
                seen_rust_tags = true
            }
            Token::Tag(tag) if tag.starts_with("skt-") => {
                info.template = Some(tag[4..].to_string());
                seen_rust_tags = true;
            }
            Token::KeyValue { key: "output_file", value } => {
                info.output_file = Some(value.to_owned());
                seen_rust_tags = true;
            }
            _ => seen_other_tags = true,
        }
    }

    info.is_rust &= !seen_other_tags || seen_rust_tags;

    info
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum Token<'a> {
    KeyValue { key: &'a str, value: &'a str },
    Tag(&'a str),
}

fn parse_token(input: &str) -> Option<(Token, &str)> {
    let mut key_residue = consume_separator(input);
    let key;
    (key, key_residue) = parse_key(key_residue)?;

    match parse_assignment(key_residue) {
        Some((value, value_residue)) => Some((Token::KeyValue { key, value }, value_residue)),
        None => Some((Token::Tag(key), key_residue)),
    }
}

fn parse_assignment(input: &str) -> Option<(&str, &str)> {
    let mut residue = consume_separator(input);
    residue = parse_equal_sign(residue)?;
    residue = consume_separator(residue);
    parse_value(residue)
}

fn parse_key(input: &str) -> Option<(&str, &str)> {
    let key_end = match input.find(|c| !is_key_character(c)) {
        Some(key_end) => key_end,
        None => {
            if input.is_empty() {
                return None;
            } else {
                return Some((input, ""));
            }
        }
    };

    let (key, residue) = input.split_at(key_end);
    if key.is_empty() {
        return None;
    }

    Some((key, residue))
}

fn parse_equal_sign(input: &str) -> Option<&str> {
    if input.is_empty() {
        return None;
    }

    let (assignment, residue) = input.split_at(1);
    if assignment == "=" {
        Some(residue)
    } else {
        None
    }
}

fn parse_value(input: &str) -> Option<(&str, &str)> {
    if input.is_empty() {
        return None;
    }

    let (opening_mark, mut residue) = input.split_at(1);
    if opening_mark != "\"" {
        return None;
    }

    let value;
    (value, residue) = residue.split_at(residue.find('\"')?);
    if residue.is_empty() {
        return None;
    }

    let closing_mark;
    (closing_mark, residue) = residue.split_at(1);
    if closing_mark != "\"" {
        return None;
    }

    Some((value, residue))
}

fn consume_separator(input: &str) -> &str {
    let separator_end = match input.find(is_special_character) {
        Some(next_token) => next_token,
        None => return input,
    };

    input.get(separator_end..).unwrap_or(input)
}

fn is_special_character(c: char) -> bool {
    is_key_character(c) || c == '\"' || c == '='
}

fn is_key_character(c: char) -> bool {
    c == '_' || c == '-' || c.is_alphanumeric()
}

#[derive(Debug)]
pub struct CodeBlockInfo {
    is_rust: bool,
    should_panic: bool,
    ignore: bool,
    compile_fail: bool,
    no_run: bool,
    is_old_template: bool,
    template: Option<String>,
    output: bool,
    output_file: Option<String>,
}

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct Test {
    pub(crate) name: String,
    pub(crate) text: Vec<String>,
    pub(crate) ignore: bool,
    pub(crate) compile_fail: bool,
    pub(crate) no_run: bool,
    pub(crate) should_panic: bool,
    pub(crate) template: Option<String>,
    pub(crate) hash: String,
    pub(crate) output: bool,
    pub(crate) output_file: Option<PathBuf>,
}

/// Just like Rustdoc, ignore a "#" sign at the beginning of a line of code.
/// These are commonly an indication to omit the line from user-facing
/// documentation but include it for the purpose of playground links or skeptic
/// testing.
#[allow(clippy::manual_strip)] // Relies on str::strip_prefix(), MSRV 1.45
fn clean_omitted_line(line: &str) -> &str {
    // XXX To silence depreciation warning of trim_left and not bump rustc
    // requirement upto 1.30 (for trim_start) we roll our own trim_left :(
    let trimmed = if let Some(pos) = line.find(|c: char| !c.is_whitespace()) {
        &line[pos..]
    } else {
        line
    };

    if trimmed.starts_with("# ") {
        &trimmed[2..]
    } else if line.trim() == "#" {
        // line consists of single "#" which might not be followed by newline on windows
        &trimmed[1..]
    } else {
        line
    }
}

/// Creates the Rust code that this test will be operating on.
pub fn create_test_input(lines: &[String]) -> String {
    lines
        .iter()
        .map(|s| clean_omitted_line(s).to_owned())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::{parse_token, Token};

    #[test]
    fn token_list_parsing() {
        let mut input = " rust key = \"value\" cool_tag";
        let mut token;

        (token, input) = parse_token(input).unwrap();
        assert_eq!(token, Token::Tag("rust"));

        (token, input) = parse_token(input).unwrap();
        assert_eq!(
            token,
            Token::KeyValue {
                key: "key",
                value: "value"
            }
        );

        (token, input) = parse_token(input).unwrap();
        assert_eq!(token, Token::Tag("cool_tag"));

        assert!(parse_token(input).is_none());
        assert_eq!(input, "");
    }

    #[test]
    fn basic_token_parsing() {
        let mut input = "_-a42:/=helo%$\"";
        let mut token;

        (token, input) = parse_token(input).unwrap();
        assert_eq!(token, Token::Tag("_-a42"));

        assert!(parse_token(input).is_none());

        let mut input = "tag";

        (token, input) = parse_token(input).unwrap();
        assert_eq!(token, Token::Tag("tag"));

        assert!(parse_token(input).is_none());

        let key_value_pairs = [
            r#"key="value""#,
            r#" key="value""#,
            r#"key ="value""#,
            r#" key ="value""#,
            r#"key= "value""#,
            r#" key= "value""#,
            r#"key = "value""#,
            r#" key = "value""#,
            r#"key="value" "#,
            r#" key="value" "#,
            r#"key ="value" "#,
            r#" key ="value" "#,
            r#"key= "value" "#,
            r#" key= "value" "#,
            r#"key = "value" "#,
            r#" key = "value" "#,
        ];

        for pair in key_value_pairs {
            let residue;
            (token, residue) = parse_token(pair).unwrap();
            assert_eq!(
                token,
                Token::KeyValue {
                    key: "key",
                    value: "value"
                }
            );

            assert!(parse_token(residue).is_none());
        }
    }

    #[test]
    fn invalid_tag_parsing() {
        let inputs = ["=", "!@#$%^&*()", " = value"];

        for input in inputs {
            assert!(parse_token(input).is_none());
        }
    }
}
